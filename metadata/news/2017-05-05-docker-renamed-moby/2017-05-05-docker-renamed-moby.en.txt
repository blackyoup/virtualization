Title: app-virtualization/docker has been renamed to app-virtualization/moby
Author: Arnaud Lefebvre <a.lefebvre@outlook.fr>
Content-Type: text/plain
Posted: 2017-05-05
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-virtualization/docker

Docker is splitting its project into several projects and
the docker core project has been renamed to moby.

You can read more here: https://mobyproject.org/#moby-and-docker

You can either install app-virtualization/moby or the docker set.
Binaries names don't change, only the project name:

* cave resolve \!app-virtualization/docker
* cave resolve docker
